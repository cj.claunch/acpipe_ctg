"""
Created on Mon Jan 30 13:56:32 2017

@author: cheryl
"""
# import python man library
import re
import copy
import csv
import json

# import acpipe library
import acpipe_acjson.acjson as ac


# function
def ctgtxt2acjson(s_runid, s_itxtctg, s_opath="./", s_layouttype="8|12", s_runtype="acpipe_ctg"):
    """
    input:
        s_runid: the plate barcode
        s_itxtctg: the ctg input txt file inclusive path
        s_opath: output path to which the acjson file will be written
        s_layouttype: well plate layout. default is "8|12" which is a 96 well plate.
        s_runtype: a string who specifies the assay coordinate pipeline element (the acpipe library name).

    output:
        ac.json file and d_acjson object

    description:
        transforms ctg txt output in to assay coordinate json format
    """
    # generate empty acjson dictionary
    ctg_dict = ac.acbuild(s_layouttype=s_layouttype, s_runid=s_runid, s_runtype=s_runtype)

    # set variables
    b_data = False
    i_coor = 0
    # open file handle
    print("{}: process {}".format(s_runtype, s_runid))
    with open(s_itxtctg, 'r', newline='') as fh_ctg:
        reader_ctg = csv.reader(fh_ctg, delimiter = '\t')
        # read row
        for ls_row in reader_ctg:
            # the case header
            if 'Raw (1)' in ls_row: # Raw is the indicator that this is the header
                b_data = True # this sets the flag that we found the header, next row will be data
            # the case data row
            elif b_data:
                # clean out the data row of white spaces
                ls_row = [re.sub(r'\s','', s_element) for s_element in ls_row]  # get rid of whitespace spaces
                lo_row = [ac.retype(s_element) for s_element in ls_row if len(s_element) > 0]  # get rid of empty elememnt and turn string into an agreeable variable type
                if len(lo_row) > 0:
                    # extract and write raw value
                    for o_element in lo_row:
                        i_coor += 1
                        d_record = {}
                        d_record.update({"CTG": ac.d_RECORD})
                        d_record["CTG"].update({"raw": o_element})
                        ctg_dict = ac.acfuseaxisrecord(d_acjson=ctg_dict, s_coor=str(i_coor), d_record=d_record, s_axis="endpoint", b_deepcopy=False)
                #  this is not a data row
                else:
                    pass
            # the case not yet header found thus not data
            else:
                pass
    # log
    s_ifile = s_itxtctg.split("/")[-1]
    ctg_dict.update({"log":"{} | ctgtxt2acjson > {}".format(s_ifile,ctg_dict["acid"])})
    # write acjson file
    with open(s_opath+ctg_dict["acid"], 'w') as fh_ctg:
        json.dump(ctg_dict, fh_ctg, indent=4, sort_keys=True)
    # return acjson object
    return(ctg_dict)


if __name__ == "__main__":
    # this code is only executed if file is run as script
    ctgtxt2acjson(s_runid="cellineDrug1Drug2CtgTime72h_20120712", s_itxtctg="celllineDrugsCtgTime72h_20120712.txt", s_opath="./", s_layouttype="8|12", s_runtype="acpipe_ctg")
