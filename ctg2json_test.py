#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 13:56:32 2017

@author: cheryl
"""
import copy
import re
import csv
import json
import pandas as pd
import acpipe_acjson.acjson as ac

#Build acjson dictionary
ctg_dict = ac.acbuild(s_layouttype = "8|12",s_runid= "GV_647V_C-D+B_120712T72", s_runtype= "acpipe_ctg")  
#Build data record/entry dictionary
d_entry = {}
d_entry.update({"conc":None})
d_entry.update({"concUnit":None})
d_entry.update({"time": 10})
d_entry.update({"timeUnit":"min"})


# read in ctg tsv
#b_header = False
b_data = False
i_coor = 0
#fh = open('../../data_ctg/72h/GV_647V_C-D+B_120712T72.txt')
with open('../../data_ctg/72h/GV_647V_C-D+B_120712T72.txt', 'r', newline='') as fh_ctg:
    reader_ctg = csv.reader(fh_ctg, delimiter = '\t')
    for row in reader_ctg:
        if 'Raw (1)' in row: # Raw is the indicator that this is the header
            # handles the header
            #b_header = True  # this sets the flag that we found the header, next row will be data
            b_data = True
        elif b_data:
            # clean out the data row of white spaces
            row2 = [re.sub(r'\W','', element) for element in row]
            row3 = [element for element in row2 if len(element) > 0]
            if len(row3) > 0:  
                #extract and write raw value
                for element in row3:
                    i_coor += 1
                    d_entry.update({'raw':float(element)})
                    d_record = copy.deepcopy(d_entry)
                    ctg_dict[str(i_coor)]["endpoint"] = {}
                    ctg_dict[str(i_coor)]["endpoint"].update({"ctg":d_record})
            else:
                # this is not a data row
                pass
        else:
            # the case not yet header found thus not data
            pass

# write acjson file 
with open("ctg_ac.json", 'w') as fh_ctg_ac:
    json.dump(ctg_dict, fh_ctg_ac, indent = 4, sort_keys=True)
    


'''
rownames = ['A', 'B', 'C','D','E','F','G','H'] 
columnnames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
df = pd.DataFrame(ctg_list, index = rownames, columns = columnnames)
df_index = df.index
value_list = list()
row_list = list()
col_list = list()
#pos_dict = [{colname:row[i] for i, colname in enumerate(df.columns)}] #for row in df.iterrows()]
pos_dict = [{k:df.values[i][v] for v,k in enumerate(df.columns)} for i in range(len(df))]
pos_dict2 = [{df.index[i]:df.values[i][v] for v,k in enumerate(df.columns)} for i in range(len(df))]
conv_json = json.dumps(pos_dict)
print(conv_json, df)
#for col, row in df.iteritems():
#    for value in row:
#        for r in rownames:
#            print(rownames)
#        value_list.append(value)
#        row_list.append(row.index)
        #dict_value = {row:value}
        #pos_dict.update(dict_value)
        #pos_dict.update(col)
#print(row_list)

        #print(row,value)
    #df_lookup = df.lookup(index, columns)
'''
'''
result = []
for row, col in zip(rownames, columnnames):
    print(row, col, df.get_value(row,col))
    #result.append(df.get_value(row,col))
print(result)
'''
#print(df)

#make a python dictionary with keys being well number and values being ctg values
#convert to pandas dataframe 
#name rows
# give header  
# convert tsv to json object
'''
#JSON to Python
#Reading json means converting json into a python value (object). json library parses json into a dictionary 
#or list in python. In order to do that, we use the loads() function (load from a string).
json_string = '{"first_name": "Guido", "last_name":"Rossum"}'
jsontopython = json.loads(json_string)
print(jsontopython['last_name'])

#Python to JSON
#output is data representation of the object(Dictionary). Method dumps() was the key to such operation.
#JSON cannot store all types of Python objects, but only te following type: lists, dictionaries, booleans, numbers, character strings, and none.Other types need to be converted in order to be stored in json.
pythondictionary = {'name':'Bob', 'age':44, 'isEmployed':True}
dictionarytojson = json.dumps(pythondictionary)
'''

# write json file
